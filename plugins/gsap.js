import gsap from 'gsap/dist/gsap'
import ScrollTrigger from 'gsap/dist/ScrollTrigger'
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin'

if (process.client) {
  gsap.registerPlugin(ScrollTrigger)
  gsap.registerPlugin(ScrollToPlugin)
}
export default ({ app }, inject) => {
  // Inject $hello(msg) in Vue, context and store.
  inject('anim', (elemClass) => {
    const elements = gsap.utils.toArray(elemClass)

    elements.forEach((element) => {
      const show = gsap.fromTo(
        element,
        {
          autoAlpha: 0,
          y: 100,
          scaleY: 0.8,
        },
        {
          duration: 0.8,
          ease: 'Power2.easeOut',
          autoAlpha: 1,
          y: 0,
          scaleY: 1,
        }
      )
      ScrollTrigger.create({
        trigger: element,
        animation: show,
        // markers: true,
        // toggleActions: 'play pause complete pause',
      })
    })
  })
  inject('delayedAnim', (elemClass) => {
    const wrappers = document.querySelectorAll(elemClass)

    wrappers.forEach((wrapper) => {
      const elements = gsap.utils.toArray(wrapper.children)
      elements.forEach((element, i) => {
        const show = gsap.fromTo(
          element,
          {
            autoAlpha: 0,
            y: 40,
          },
          {
            duration: 0.8,
            ease: 'Power2.easeOut',
            autoAlpha: 1,
            y: 0,
            delay: i / 4,
          }
        )
        ScrollTrigger.create({
          trigger: element,
          animation: show,
        })
      })
    })
  })
  inject('childrenAnim', (elemClass) => {
    const wrappers = document.querySelectorAll(elemClass)

    wrappers.forEach((wrapper) => {
      const elements = gsap.utils.toArray(wrapper.children)
      elements.forEach((element) => {
        const show = gsap.fromTo(
          element,
          {
            autoAlpha: 0,
            y: 20,
          },
          {
            duration: 0.8,
            ease: 'Power2.easeOut',
            autoAlpha: 1,
            y: 0,
          }
        )
        ScrollTrigger.create({
          trigger: element,
          animation: show,
        })
      })
    })
  })
  inject('scaleY', (elemClass) => {
    const element = gsap.utils.toArray(elemClass)

    const scale = gsap.fromTo(
      element,
      {
        y: 100,
        scaleY: 0.6,
      },
      {
        duration: 0.8,
        ease: 'Power2.easeOut',
        y: 0,
        scaleY: 1,
        transformOrigin: 'bottom',
      }
    )
    ScrollTrigger.create({
      trigger: element,
      animation: scale,
      scrub: 2,
      toggleActions: 'play pause complete pause',
    })
  })
  inject('momentumScroll', () => {
    // const viewport = document.querySelector('#gs-viewport')
    const scrollContainer = document.querySelector('#gs-scroll-container')
    // const height = scrollContainer.clientHeight
    // document.body.style.height = height + 'px'

    let height
    function setHeight() {
      height = scrollContainer.clientHeight
      document.body.style.height = height + 'px'
    }
    ScrollTrigger.addEventListener('refreshInit', setHeight)

    gsap.to(scrollContainer, {
      y: () => -(height - document.documentElement.clientHeight),
      ease: 'none',
      scrollTrigger: {
        trigger: document.body,
        start: 'top top',
        end: 'bottom bottom',
        scrub: 0.75,
        invalidateOnRefresh: true,
        ease: 'Power2.easeOut',
        // scroller: scrollBody,
      },
    })
  })
  // inject('scrollerProxy', (id) => {
  //   // Tell ScrollTrigger to use these proxy getter/setter methods for the "body" element:
  //   ScrollTrigger.scrollerProxy(document.body, {
  //     scrollTop(value) {
  //       if (arguments.length) {
  //         id.scrollTop = value // setter
  //       }
  //       return id.scrollTop // getter
  //     },
  //     getBoundingClientRect() {
  //       return {
  //         top: 0,
  //         left: 0,
  //         width: window.innerWidth,
  //         height: window.innerHeight,
  //       }
  //     },
  //   })
  // })
}
